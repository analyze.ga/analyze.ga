module.exports = {
  extends: 'xo/esnext',
  rules: {
    semi: ['error', 'never'],
    indent: ['error', 2]
  }
}
